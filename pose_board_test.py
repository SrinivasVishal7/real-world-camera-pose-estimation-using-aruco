import numpy as np
import os
import yaml
import math

import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

import cv2.cv2 as cv2

method1 = False
AbsoluteValueAngles = True
# cam1 = True
cam1 = False


def yawpitchrolldecomposition(R):
    sin_x    = math.sqrt(R[2,0] * R[2,0] +  R[2,1] * R[2,1])    
    validity  = sin_x < 1e-6
    if not validity:
        z1    = math.atan2(R[2,0], R[2,1])     
        x      = math.atan2(sin_x,  R[2,2])    
        z2    = math.atan2(R[0,2], -R[1,2])    
    else: 
        z1    = 0                              
        x      = math.atan2(sin_x,  R[2,2])    
        z2    = 0                              

    return np.array([[z1], [x], [z2]])


if not os.path.exists('/home/ml/Downloads/aruco_test/intrinsics/test_cam2.yml'):
    print("caliberate camera..")
    exit()
else:
    with open('/home/ml/Downloads/aruco_test/intrinsics/test_cam2.yml', 'r') as stream:
        val = yaml.load(stream)
    cameraMatrix = np.asarray(val['M1']['data']).reshape(3,3)
    distCoeffs = np.asarray(val['D1']['data']).reshape(1,14)
    stream.close()
    if cameraMatrix is None or distCoeffs is None:
        print("Calibration issue..")
        exit()

ARUCO_PARAMETERS = cv2.aruco.DetectorParameters_create()
ARUCO_DICT = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)

board = cv2.aruco.GridBoard_create(
        markersX=4,
        markersY=6,
        markerLength=0.03,
        markerSeparation=0.01,
        dictionary=ARUCO_DICT)

rvecs, tvecs = None, None

cam = cv2.VideoCapture(0)

while(cam.isOpened()):
    ret, QueryImg = cam.read()
    if ret == True:
        QueryImg = QueryImg[:, :, 1]
        gray = QueryImg.copy()
    
        corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray, ARUCO_DICT, parameters=ARUCO_PARAMETERS)
  
        corners, ids, rejectedImgPoints, recoveredIds = cv2.aruco.refineDetectedMarkers(
                image = gray,
                board = board,
                detectedCorners = corners,
                detectedIds = ids,
                rejectedCorners = rejectedImgPoints,
                cameraMatrix = cameraMatrix,
                distCoeffs = distCoeffs)   

        gray = cv2.aruco.drawDetectedMarkers(gray, corners)

        if ids is not None and len(ids) > 15:
            pose, rvec, tvec = cv2.aruco.estimatePoseBoard(corners, ids, board, cameraMatrix, distCoeffs)

            if pose:
                gray = cv2.aruco.drawAxis(gray, cameraMatrix, distCoeffs, rvec, tvec, 0.1)
                    
                # add Pose estimation code here 
                            
                #print(rvec.shape)
                #print(rvec)
                #print(tvec)
                rmat = cv2.Rodrigues(rvec)[0]
                print rmat.shape
                print tvec.shape
                print rmat
                print tvec
                cam_pos = np.negative(np.matrix(rmat).T) * np.matrix(tvec)
                # rmat, _ = cv2.Rodrigues(rvec)
                # R, _ = cv2.Rodrigues(rvec)
                # cam_pos = np.dot(cv2.transpose(-R), cv2.transpose(tvec))

                #rvec,tvec=cv2.aruco.estimatePoseSingleMarkers(corners, 0.17, np.asarray(val['M1']['data']).reshape(3,3), np.asarray(val['D1']['data']).reshape(1,14)) 
                # rmat, J = cv2.Rodrigues(np.array(rvec)) 
                # cam_pos=np.dot(-rmat.T,tvec[0].T)
                # cam_pos = np.matrix(rmat).T * np.negative(np.matrix(tvec).T)
                # cam_pos = -np.matrix(rmat).T * np.matrix(tvec).T
                #print(rmat)
                print(cam_pos)
                P = np.hstack((np.matrix(rmat),np.matrix(tvec)))
                    
                if method1:
                    eul_angles_rads = -cv2.decomposeProjectionMatrix(P)[6]
                    eul_angles_degs = 180 * eul_angles_rads/math.pi 
                    eul = eul_angles_rads
                        
                    # yawpitchroll_angles = -180*yawpitchrolldecomposition(rmat)/math.pi
                    # yawpitchroll_angles[0,0] = (360-yawpitchroll_angles[0,0])%360 # change rotation sense if needed, comment this line otherwise
                    # yawpitchroll_angles[1,0] = yawpitchroll_angles[1,0]+90
                    # print("Camera Yaw, pitch, roll :", yawpitchroll_angles)
                         
                    if AbsoluteValueAngles:
                        yaw    = (180*eul[1,0]/math.pi)%360 
                        pitch  = (180*((eul[0,0]+math.pi/2)*math.cos(eul[1,0]))/math.pi)%360
                        roll   = (180*( (-(math.pi/2)-eul[0,0])*math.sin(eul[1,0]) + eul[2,0] )/math.pi)%360
                        print("\nCamera Method 1 : Yaw, pitch, roll : ", yaw, pitch, roll)
                    else:
                        yaw    = 180*eul[1,0]/math.pi
                        pitch  = 180*((eul[0,0]+math.pi/2)*math.cos(eul[1,0]))/math.pi
                        roll   = 180*( (-(math.pi/2)-eul[0,0])*math.sin(eul[1,0]) + eul[2,0] )/math.pi
                        print("\nCamera Method 1 : Yaw, pitch, roll : ", yaw, pitch, roll)

                else:
                    yawpitchroll_angles = -180*yawpitchrolldecomposition(rmat)/math.pi
                    #yawpitchroll_angles = 180*yawpitchrolldecomposition(rmat)/math.pi
                    yawpitchroll_angles[0,0] = (360-yawpitchroll_angles[0,0])%360 # change rotation sense if needed, comment this line otherwise
                    yawpitchroll_angles[1,0] = yawpitchroll_angles[1,0]+90
                    print("\nCamera Method 2 : Yaw, pitch, roll :", yawpitchroll_angles)

                print("\nCamera Translation (XYZ) : ",cam_pos)
                print("\nExtrinsic Matrix: ",P)
                print (" --------------------------------------------------------- ")

                angles_enu = -yawpitchroll_angles*0.0174533
                # position_enu = cam_pos
                # position_enu[0,0] = cam_pos[1,0]
                # position_enu[1,0] = -1*cam_pos[0,0]
                # position_enu[2,0] = cam_pos[2,0]
                # print position_enu
                print cam_pos
                if cam1:
                    # print 'cam1_pose_.position.x =', position_enu[0,0],';'
                    # print 'cam1_pose_.position.y =', position_enu[1,0],';'
                    # print 'cam1_pose_.position.z =', position_enu[2,0],';' 
                    print 'cam1_pose_.position.x =', cam_pos[1,0],';'
                    print 'cam1_pose_.position.y =', -1*cam_pos[0,0],';'
                    print 'cam1_pose_.position.z =', cam_pos[2,0],';'
                    print 'cam1_pose_.orientation = tf::createQuaternionMsgFromRollPitchYaw(', angles_enu[2,0],',', angles_enu[1,0],',', angles_enu[0,0],');'
                    print cam_pos[1,0], -1*cam_pos[0,0], cam_pos[2,0], angles_enu[0,0], angles_enu[1,0], angles_enu[2,0]
                else:
                    print 'cam2_pose_.position.x =', cam_pos[1,0],';'
                    print 'cam2_pose_.position.y =', -1*cam_pos[0,0],';'
                    print 'cam2_pose_.position.z =', cam_pos[2,0],';'
                    print 'cam2_pose_.orientation = tf::createQuaternionMsgFromRollPitchYaw(', angles_enu[2,0],',', angles_enu[1,0],',', angles_enu[0,0],');'
                    print cam_pos[1,0], -1*cam_pos[0,0], cam_pos[2,0], angles_enu[0,0], angles_enu[1,0], angles_enu[2,0]

        cv2.imshow('QueryImage', gray)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
