import yaml
import numpy as np
import math

#Comment this if you are not using ROS
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

import cv2.cv2 as cv2

cap = cv2.VideoCapture(0)

# -------------- Flags ------------------ #
method1 = False
AbsoluteValueAngles = True
cam1 = True
# cam1 = False

# --------------- Utils ----------------- #
def get_intrinsics(file_path):
    with open(file_path, 'r') as stream:
        val = yaml.load(stream)
    stream.close()
    return val 

def yawpitchrolldecomposition(R):
    sin_x    = math.sqrt(R[2,0] * R[2,0] +  R[2,1] * R[2,1])    
    validity  = sin_x < 1e-6
    if not validity:
        z1    = math.atan2(R[2,0], R[2,1])     
        x      = math.atan2(sin_x,  R[2,2])    
        z2    = math.atan2(R[0,2], -R[1,2])    
    else: 
        z1    = 0                              
        x      = math.atan2(sin_x,  R[2,2])    
        z2    = 0                              

    return np.array([[z1], [x], [z2]])

# --------------------- Main ------------------- #

if __name__ == '__main__':
    if len(sys.argv) > 1:
        val = get_intrinsics(sys.argv[1])
    else:
        print("\nEnter File Path as argument\nUsage : python3 aruco_marker_test.py [CAMERA INTRINSICS FILE PATH]\n")
        exit(0)
        
    while (True):
        ret, frame = cap.read()
        frame = frame[:, :, 1]
        gray = frame.copy()
        aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        parameters = cv2.aruco.DetectorParameters_create()

        corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

        font = cv2.FONT_HERSHEY_SIMPLEX

        if np.all(ids != None):
            rvec, tvec ,_ = cv2.aruco.estimatePoseSingleMarkers(corners, 0.17, np.asarray(val['M1']['data']).reshape(3,3), np.asarray(val['D1']['data']).reshape(1,14)) 
            print(rvec.shape)
            print(tvec.shape)
            #print(rvec)
            #print(tvec)
            # rmat = cv2.Rodrigues(rvec)[0]
            #cam_pos = np.negative(np.matrix(rmat).T) * np.matrix(tvec).T
            # rmat, _ = cv2.Rodrigues(rvec)
            #R, _ = cv2.Rodrigues(rvec)
            #cam_pos = np.dot(cv2.transpose(-R), cv2.transpose(tvec))

            #rvec,tvec=cv2.aruco.estimatePoseSingleMarkers(corners, 0.17, np.asarray(val['M1']['data']).reshape(3,3), np.asarray(val['D1']['data']).reshape(1,14)) 
            rmat, J = cv2.Rodrigues(np.array(rvec)) 
            cam_pos=np.dot(-rmat.T,tvec[0].T)
            # cam_pos = np.matrix(rmat).T * np.negative(np.matrix(tvec).T)
            # cam_pos = -np.matrix(rmat).T * np.matrix(tvec).T
            #print(rmat)
            print(cam_pos)
            P = np.hstack((np.matrix(rmat),np.matrix(tvec).T))
            
            if method1:
                eul_angles_rads = -cv2.decomposeProjectionMatrix(P)[6]
                eul_angles_degs = 180 * eul_angles_rads/math.pi 
                eul = eul_angles_rads
                
                # yawpitchroll_angles = -180*yawpitchrolldecomposition(rmat)/math.pi
                # yawpitchroll_angles[0,0] = (360-yawpitchroll_angles[0,0])%360 # change rotation sense if needed, comment this line otherwise
                # yawpitchroll_angles[1,0] = yawpitchroll_angles[1,0]+90
                # print("Camera Yaw, pitch, roll :", yawpitchroll_angles)
                 
                if AbsoluteValueAngles:
                    yaw    = (180*eul[1,0]/math.pi)%360 
                    pitch  = (180*((eul[0,0]+math.pi/2)*math.cos(eul[1,0]))/math.pi)%360
                    roll   = (180*( (-(math.pi/2)-eul[0,0])*math.sin(eul[1,0]) + eul[2,0] )/math.pi)%360
                    print("\nCamera Method 1 : Yaw, pitch, roll : ", yaw, pitch, roll)
                else:
                    yaw    = 180*eul[1,0]/math.pi
                    pitch  = 180*((eul[0,0]+math.pi/2)*math.cos(eul[1,0]))/math.pi
                    roll   = 180*( (-(math.pi/2)-eul[0,0])*math.sin(eul[1,0]) + eul[2,0] )/math.pi
                    print("\nCamera Method 1 : Yaw, pitch, roll : ", yaw, pitch, roll)

            else:
                yawpitchroll_angles = -180*yawpitchrolldecomposition(rmat)/math.pi
                #yawpitchroll_angles = 180*yawpitchrolldecomposition(rmat)/math.pi
                yawpitchroll_angles[0,0] = (360-yawpitchroll_angles[0,0])%360 # change rotation sense if needed, comment this line otherwise
                yawpitchroll_angles[1,0] = yawpitchroll_angles[1,0]+90
                print("\nCamera Method 2 : Yaw, pitch, roll :", yawpitchroll_angles)

            print("\nCamera Translation (XYZ) : ",cam_pos)
            print("\nExtrinsic Matrix: ",P)
            print (" --------------------------------------------------------- ")

            angles_enu = -yawpitchroll_angles*0.0174533
            # position_enu = cam_pos
            # position_enu[0,0] = cam_pos[1,0]
            # position_enu[1,0] = -1*cam_pos[0,0]
            # position_enu[2,0] = cam_pos[2,0]
            # print position_enu
            print cam_pos
            if cam1:
                # print 'cam1_pose_.position.x =', position_enu[0,0],';'
                # print 'cam1_pose_.position.y =', position_enu[1,0],';'
                # print 'cam1_pose_.position.z =', position_enu[2,0],';' 
                print 'cam1_pose_.position.x =', cam_pos[1,0],';'
                print 'cam1_pose_.position.y =', -1*cam_pos[0,0],';'
                print 'cam1_pose_.position.z =', cam_pos[2,0],';'
                print 'cam1_pose_.orientation = tf::createQuaternionMsgFromRollPitchYaw(', angles_enu[2,0],',', angles_enu[1,0],',', angles_enu[0,0],');'
                print cam_pos[1,0], -1*cam_pos[0,0], cam_pos[2,0], angles_enu[0,0], angles_enu[1,0], angles_enu[2,0]
            else:
                print 'cam2_pose_.position.x =', cam_pos[1,0],';'
                print 'cam2_pose_.position.y =', -1*cam_pos[0,0],';'
                print 'cam2_pose_.position.z =', cam_pos[2,0],';'
                print 'cam2_pose_.orientation = tf::createQuaternionMsgFromRollPitchYaw(', angles_enu[2,0],',', angles_enu[1,0],',', angles_enu[0,0],');'
                print cam_pos[1,0], -1*cam_pos[0,0], cam_pos[2,0], angles_enu[0,0], angles_enu[1,0], angles_enu[2,0]

                # cam1_pose_.position.y = 0.7579195684;
                # cam1_pose_.position.z = 0.7578781731;
                # cam1_pose_.orientation = tf::createQuaternionMsgFromRollPitchYaw(-1*3.43476639*0.0174533, 50.06646125*0.0174533, -1*83.29924628*0.0174533);

            for i in range(0, ids.size):
                cv2.aruco.drawAxis(gray, np.asarray(val['M1']['data']).reshape(3,3), np.asarray(val['D1']['data']).reshape(1,14), rvec[i], tvec[i], 0.1)
                cv2.aruco.drawDetectedMarkers(gray, corners) 

            strg = ''
            for i in range(0, ids.size):
                strg += str(ids[i][0])+', '

            cv2.putText(gray, "Id: " + strg, (0,64), font, 1, 255,2,cv2.LINE_AA)

        else:
            cv2.putText(gray, "No Ids", (0,64), font, 1, 255,2,cv2.LINE_AA)

        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
        
